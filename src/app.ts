import express, {Application, Request, Response, NextFunction} from 'express';
import morgan from 'morgan';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';

import dotenv from 'dotenv';

import userRoutes from './api/routes/user'; 
import productRoutes from './api/routes/product';
import orderRoutes from './api/routes/order';

dotenv.config({ path: '.env' });

mongoose.connect(`${process.env.MONGO_ATLAS_CONNECTION}`, 
{ 
    useNewUrlParser: true,
    useCreateIndex: true
})
.then(result => console.info('Connected to Mongo'))
.catch(err => console.error(err));

const app: Application = express();

app.use(morgan('dev'));
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);

app.use((req: Request, res: Response, next: NextFunction) => {
    const error: any = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use((error:any, req: Request, res: Response, next: NextFunction) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        },
    })
});

export default app;