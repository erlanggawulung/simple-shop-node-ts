import bcrypt from 'bcrypt';
import { userModel, IUser } from '../models/user';
import jwt from 'jsonwebtoken';
    
const authUser = async (email: String, password: String) => {
    const existingUser: any = await userModel.find({email: email}).exec();
    if (existingUser.length < 1) {
        throw new Error('Login failed');
    }
    const result = await bcrypt.compare(password, existingUser[0].password);
    if (result) {
        const token = jwt.sign(
            {
                email: existingUser[0].email,
                userId: existingUser[0]._id
            }, 
            `${process.env.JWT_KEY}`,
            {
                expiresIn: "1h",
            }
        );
        return token;
    } else {
        throw new Error('Login failed');
    }
};

const addUser = async (email: String, password: String) => {
    const existingUser = await userModel.find({email: email}).exec();
    if (existingUser.length >= 1) {
        throw new Error('Email already registered');
    } else {
        const hash = await bcrypt.hash(password, 10);
        const newUser: IUser = new userModel({
            email: email,
            password: hash,
        });
        await newUser.save();
        return newUser;
    }
}

export {authUser, addUser}