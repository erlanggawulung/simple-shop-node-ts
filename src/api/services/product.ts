import { productModel, IProduct } from '../models/product';

const getAllProduct = async () => {
    const products = await productModel.find()
    .select('_id name price productImage')
    .exec();
    return products;
}

const createProduct = async (name: string, price: number, productImage: string) => {
    const newProduct: IProduct = new productModel({
        name: name,
        price: price,
        productImage: productImage
    });
    await newProduct.save();
    return newProduct;
}

const getProduct = async (productId: string) => {
    const product = await productModel.findById(productId)
    .select('name price productImage _id')
    .exec();
    return product;
}

const patchProduct = async (productId: string, body: any) => {
    const updateOps: any = {};
    for (const ops of body) {
        updateOps[ops.propName] = ops.value;
    }

    const result = await productModel.updateOne({_id: productId}, {$set: updateOps})
    .exec();
    return result;
}

const deleteProduct = async (productId: string) => {
    const result = await productModel.deleteOne({_id: productId})
    .exec();
    return result;
}

export {getAllProduct, createProduct, getProduct, patchProduct, deleteProduct}