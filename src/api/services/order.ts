import { orderModel, IOrder } from '../models/order';
import { productModel, IProduct } from '../models/product';

const getAllOrder = async () => {
    const orders = await orderModel.find()
    .select('product quantity _id')
    .populate('product', 'name price')
    .exec();
    return orders;
    
}

const createOrder = async (productId: string, quantiy: number, ) => {
    const product = await productModel.findById(productId)
    .select('name price productImage _id')
    .exec();
    if (product) {
        const newOrder: IOrder = new orderModel({
            product: productId,
            quantity: quantiy
        });
        await newOrder.save();
        return newOrder;   
    } else {
        throw new Error('No valid entry found provided Product ID');
    }
}

const getOrder = async (orderId: string) => {
    const order = await orderModel.findById(orderId)
    .select('product quantity _id')
    .populate('product', 'name price')
    .exec();
    return order;
}

const deleteOrder = async (orderId: string) => {
    const result = await orderModel.deleteOne({_id: orderId})
    .exec();
    return result;
}

export {getAllOrder, createOrder, getOrder, deleteOrder}