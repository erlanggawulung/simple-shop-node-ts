import {Request, Response, NextFunction} from 'express';
import jwt from 'jsonwebtoken';

const checkAuth = (req: any, res: Response, next: NextFunction) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, `${process.env.JWT_KEY}`);
        req.userData = decoded;
        next();
    } catch(err) {
        console.log({err});
        return res.status(401).json({
            message: 'Auth failed.'
        });
    }
}

export default checkAuth;