import mongoose, { Schema, Document } from 'mongoose';

interface IProduct extends Document {
    _id: String;
    name: String,
    price: Number,
    productImage: String
}

const productSchema: Schema = new Schema({
    name: { type: String, required: true },
    price: { type: Number, required: true },
    productImage: { type: String, required: true }
});

const productModel = mongoose.model<IProduct>('Product', productSchema);
export { productModel, IProduct }