import mongoose, { Schema, Document } from 'mongoose';

interface IUser extends Document {
    _id: String;
    email: String,
    password: String
}

const userSchema: Schema = new Schema({
    email: { 
        type: String, 
        required: true, 
        unique: true,
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },
    password: { type: String, required: true }
});

const userModel = mongoose.model<IUser>('User', userSchema);
export { userModel, IUser }