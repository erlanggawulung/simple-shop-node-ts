import mongoose, { Schema, Document } from 'mongoose';

interface IOrder extends Document {
    _id: String;
    product: String,
    quantity: Number
}

const orderSchema: Schema = new Schema({
    product: { type: mongoose.Schema.Types.ObjectId, ref: 'Product', required: true },
    quantity: { type: Number, default: 1 }
});

const orderModel = mongoose.model<IOrder>('Order', orderSchema);
export { orderModel, IOrder }