import express from 'express';
import {productGetAll, productCreateProduct, productGetProduct, productPatchProduct, productDeleteProduct} from '../controllers/product';
import checkAuth from '../middleware/checkAuth';
import cors from '../middleware/crossOrigin';
import uploadImage from '../middleware/uploadImage';

const router = express.Router();

router.get('/', cors, productGetAll);
router.post('/', cors, checkAuth, uploadImage.single('productImage'), productCreateProduct);
router.get('/:productId', cors, productGetProduct);
router.patch('/:productId', cors, productPatchProduct);
router.delete('/:productId', cors, productDeleteProduct);

export default router;