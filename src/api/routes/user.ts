import express from 'express';
import {userSignup, userLogin, userGetProfile} from '../controllers/user';
import checkAuth from '../middleware/checkAuth';
import cors from '../middleware/crossOrigin';

const router = express.Router();

router.post('/signup', cors, userSignup);
router.get('/profile', cors, checkAuth, userGetProfile);
router.post('/login', cors, userLogin);

export default router;