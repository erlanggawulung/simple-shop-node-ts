import express from 'express';
import {orderGetAll, orderCreateOrder, orderGetOrder, orderDeleteOrder} from '../controllers/order';
import cors from '../middleware/crossOrigin';

const router = express.Router();

router.get('/', cors, orderGetAll);
router.post('/', cors, orderCreateOrder);
router.get('/:orderId', cors, orderGetOrder);
router.delete('/:orderId', cors, orderDeleteOrder);

export default router;