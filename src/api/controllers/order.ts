import {Request, Response, NextFunction} from 'express';
import {
    getAllOrder,
    createOrder,
    getOrder,
    deleteOrder
} from '../services/order';

const orderGetAll = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const docs = await getAllOrder();
        const response = {
            count: docs.length,
            orders: docs.map(doc => {
                return {
                    product: doc.product,
                    quantity: doc.quantity,
                    _id: doc._id,
                    request: {
                        type: 'GET',
                        url: 'http://localhost:3000/orders/'+doc._id
                    }
                }
            })
        };
        res.status(200).json(response);
    } catch (e) {
        res.status(500).json({error: e});
    }
}

const orderCreateOrder = async (req: Request, res: Response, next: NextFunction) => {
    const {productId, quantity} = req.body;
    try {
        const order = await createOrder(productId, quantity);
        res.status(201).json({
            message: 'Order created successfully',
            createdOrder: {
                quantity: order.quantity,
                product: order.product,
                _id: order._id,
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/products/'+productId
                }
            }
        });
    } catch (e) {
        res.status(500) && next(e);
    }
}

const orderGetOrder = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const id = req.params.orderId;
        const order = await getOrder(id);
        if(order) {
            res.status(200).json({
                order: order,
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/orders'
                }
            });
        } else {
            res.status(404).json({message: 'No valid entry found provided ID.'});
        }
    } catch (e) {
        res.status(500) && next(e);
    }
}

const orderDeleteOrder = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const id = req.params.orderId;
        const result = await deleteOrder(id);
        console.info(result);
        res.status(200).json({
            message: 'Order deleted',
            request: {
                type: 'POST',
                url: 'http://localhost:3000/orders',
                body: {
                    productId: 'ID',
                    quantity: 'Number'
                }
            }
        });
    } catch (e) {
        res.status(500) && next(e);
    }
}

export {orderGetAll, orderCreateOrder, orderGetOrder, orderDeleteOrder}