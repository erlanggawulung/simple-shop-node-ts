import {Request, Response, NextFunction} from 'express';
import {authUser, addUser} from '../services/user';

const userSignup = async (req: Request, res: Response, next: NextFunction) => {
    const {email, password} = req.body;
    try {
        const user = await addUser(email, password);
        console.log({user});
        return res.status(201).json({
            message: 'User created.'
        });
    } catch (e) {
        res.status(500) && next(e);
    }
} 

const userLogin = async (req: Request, res: Response, next: NextFunction) => {
    const {email, password} = req.body;
    try {
        const token = await authUser(email, password);
        return res.status(200).json({
            mesage: 'Auth successful',
            token: token
        });
    } catch (e) {
        res.status(401).json({'error':{'message':'Auth failed'}});
    }
};

const userGetProfile = async (req: any, res: Response, next: NextFunction) => {
    return res.status(200).json(req.userData);
};

export {userSignup, userLogin, userGetProfile}