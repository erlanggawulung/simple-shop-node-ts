import {Request, Response, NextFunction} from 'express';
import {
    getAllProduct, 
    createProduct, 
    getProduct, 
    patchProduct, 
    deleteProduct
} from '../services/product';

const productGetAll = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const docs = await getAllProduct();
        const response = {
            count: docs.length,
            products: docs.map(doc => {
                return {
                    name: doc.name,
                    price: doc.price,
                    productImage: doc.productImage,
                    _id: doc._id,
                    request: {
                        type: 'GET',
                        url: 'http://localhost:3000/products/'+doc._id
                    }
                }
            })
        };
        res.status(200).json(response);
    } catch (e) {
        res.status(500).json({error: e});
    }
}

const productCreateProduct = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const {name, price} = req.body;
        const {path} = req.file;
        const product = await createProduct(name, price, path);
        res.status(201).json({
            message: 'Product created successfully',
            createdProduct: {
                productImage: product.productImage,
                name: product.name,
                price: product.price,
                _id: product._id,
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/products/'+product._id
                }
            }
        });
    } catch (e) {
        res.status(500) && next(e);
    }
}

const productGetProduct = async (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.productId;
    try {
        const product = await getProduct(id);
        if(product) {
            res.status(200).json({
                product: product,
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/products'
                }
            });
        } else {
            res.status(404).json({message: 'No valid entry found provided ID.'});
        }
    } catch (e) {
        res.status(500) && next(e);
    }
}

const productPatchProduct = async (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.productId;
    try {
        const result = await patchProduct(id, req.body);
        res.status(200).json({
            message: 'Product updated',
            request: {
                type: 'GET',
                url: 'http://localhost:3000/products/'+id
            }
        });
    } catch (e) {
        res.status(500) && next(e);
    }
}

const productDeleteProduct = async (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.productId;
    try {
        const result = await deleteProduct(id);
        res.status(200).json({
            message: 'Product deleted',
            request: {
                type: 'POST',
                url: 'http://localhost:3000/products',
                body: {
                    productImage: 'jpg, png',
                    name: 'String',
                    price: 'Number'
                }
            }
        });
    } catch (e) {
        res.status(500) && next(e);
    }
}

export {productGetAll, productCreateProduct, productGetProduct, productPatchProduct, productDeleteProduct}